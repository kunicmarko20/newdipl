<?php


namespace AppBundle\Block\Service;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;

class RecentBlockService extends BaseBlockService
{
    protected $em;

    public function __construct(EntityManager $em, $type, $templating)
    {
        $this->type = $type;
        $this->templating = $templating;
        $this->em = $em;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Entity Recent Blocks';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'entity'      => 'Add Entity',
            'title'    => 'Insert block Title',
            'limit'    => 5,
            'icon'    => 'fa-users',
            'template' => 'AppBundle:Block:block_core_recent.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('entity', 'url', array('required' => false)),
                array('title', 'text', array('required' => false)),
                array('limit', 'number', array('required' => false)),
                array('icon', 'text', array('required' => false)),
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        $errorElement
            ->with('settings[entity]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[title]')
                ->assertNotNull(array())
                ->assertNotBlank()
                ->assertMaxLength(array('limit' => 50))
            ->end()
            ->with('settings[limit]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[icon]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();
          
        $rows = $this->em->getRepository($settings['entity'])->findBy(array(), array('id' => 'DESC'),$settings['limit']);
        $url = explode("\\",$settings['entity']);
        
        return $this->templating->renderResponse($blockContext->getTemplate(), array(
            'playlists'     => $rows,
            'url' => end($url),
            'block'     => $blockContext->getBlock(),
            'settings'  => $settings,
        ), $response);
    }
}
