<?php

namespace AppBundle\Service;

use AppBundle\Entity\Song;
use AppBundle\Entity\Playlist;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SongService
{
    private $em;
    
    public function __construct(EntityManager $em){
        $this->em = $em;
    }
    
    public function getSongs($id){
                
        $result = $this->em->getRepository('AppBundle:Song')->findBy(array('playlist' => $id),array('position' => 'ASC','id'=>'ASC'));
        return $result;
    }
    public function getNewestSongs($limit){
        $result = $this->em->getRepository('AppBundle:Song')->findBy(array(), array('id' => 'DESC'),$limit);
        return $result;
    }
    public function addSong($playlist,$item){
        $item->setPlaylist($playlist);
        $this->em->persist($item);
        $this->em->flush();
        return $item;
    }
    public function deleteSong($id){
        
        $item = $this->em->getRepository('AppBundle:Song')->findOneById($id); 
        if ($item) {
                $this->em->remove($item);
                $this->em->flush();
                $this->em->clear();
        } else {
            throw new NotFoundHttpException("Not found");
        }
    }
}