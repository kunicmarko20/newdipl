<?php

namespace AppBundle\Service;

use AppBundle\Entity\Song;
use AppBundle\Entity\Playlist;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserService
{    
    protected $em;
    protected $user;
    protected $dir;

    public function __construct(EntityManager $em,$user,$dir) {
        $this->em = $em;
        $this->user = $user;
        $this->dir = $dir;
    }

    public function getUser($username){
        $user = $this->checkUser($username);
        if($user[0]){
           $result = $this->em->getRepository('AppBundle:Playlist')->findBy(array('user' => $user[1]));   
        } else {         
           $result = $this->em->getRepository('AppBundle:Playlist')->findBy(array('user' => $user[1],'private'=>0)); 
        }
  
        return array('user'=>$user[1],'playlists'=>$result);
    }
    public function search($string,$page){
        $offset = ($page - 1)*9;
        $playlists = $this->em->getRepository('AppBundle:Playlist')->findLike($string); 
        $users = $this->em->getRepository('AppBundle:User')->findLike($string);
        
        $count = ceil((count($playlists) + count($users)) / 9);
        
        $result = array_merge($playlists, $users);
        $result = array_slice($result,$offset,9); 
        shuffle($result);
        return array('pages'=> $count,'result'=>$result);
    }
    
    public function checkUser($username){
        $user = $this->em->getRepository('AppBundle:User')->findOneBy(array('username'=>$username)); 
        if(!$user){
            throw new NotFoundHttpException("User not found");
        }
        if(!($this->user == "anon.") && $this->user->getUsername() == $username){
            return array(true,$user);
        }
        return array(false,$user);
    }
    
    public function editUser($item,$hidden = false){
         $file = $item->getPicture();
         if(is_object($file)){
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->dir,
                $fileName
            );
            $item->setPicture($fileName);
            $hidden && $hidden != 'userdefault.jpg' ? unlink("$this->dir/$hidden") : '';
         }else if ($file === null){
            $item->setPicture('userdefault.jpg');  
         }
        $this->em->persist($item);
        $this->em->flush();
        return $item;
    }
}
