<?php

namespace AppBundle\Service;

use AppBundle\Entity\Song;
use AppBundle\Entity\Playlist;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PlaylistService
{    
    protected $em;
    protected $user;
    protected $dir;

    public function __construct(EntityManager $em,$user,$dir) {
        $this->em = $em;
        $this->user = $user;
        $this->dir = $dir;
    }
    public function managePlaylist($item,$hidden = false){
         $file = $item->getPicture();
         if(is_object($file)){
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->dir,
                $fileName
            );
            $item->setPicture($fileName);
            $hidden && $hidden != 'default.jpg' ? unlink("$this->dir/$hidden") : '';
         }else if ($file === null){
            $item->setPicture('default.jpg');  
         }

        $item->setUser($this->user);
        $this->em->persist($item);
        $this->em->flush();
        return $item;
    }
    
    public function deletePlaylist($playlist){
        if ($playlist) {
                $this->em->remove($playlist);
                $this->em->flush();
                $this->em->clear(); 
        } else {
            throw new NotFoundHttpException("Not found");
        }
    }
    
    public function repositionPlaylist($songs,$id){
        foreach($songs as $k => $song){
            $item = $this->em->getRepository('AppBundle:Song')->findOneBy(array('video' => $song,'playlist' => $id));
            $item->setPosition($k);
            $this->em->persist($item);
            $this->em->flush();
        }
        return array('message'=>'Successfully updated');
    }
}
