<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PlaylistAdmin extends Admin
{
    public function toString($object)
    {
        return $object->getName() != null ? $object->getName() : 'Playlist';
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
         ->with('Content', array('description' => 'This section contains general content for the web page'))
            ->add('name', 'text')
            ->add('private','checkbox', array('required' => false))
        ->end()

        ->with('Meta data')
            ->add('user', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\User',
                'property' => 'username',
            ))
        ->end();
        
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
          ->add('user', null, array(), 'entity', array(
                'class'    => 'AppBundle\Entity\User',
                'property' => 'username',
            ));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            ->add('user.username')
        ->add('private', 'boolean', ["editable" => true])
                ->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array()
            )
        ))
        ;
    }
}

