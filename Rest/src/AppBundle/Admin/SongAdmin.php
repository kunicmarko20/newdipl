<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SongAdmin extends Admin
{
    public function toString($object)
    {
        return $object->getTitle() != null ? $object->getTitle() : 'Song';
    }
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
         ->with('Content', array('description' => 'This section contains general content for the web page'))
            ->add('title', 'text')
            ->add('video')
        ->end()
        
        ->with('Meta data')
            ->add('playlist', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\Playlist',
                'property' => 'name',
            ))
        ->end();
        
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title')
          ->add('playlist', null, array(), 'entity', array(
                'class'    => 'AppBundle\Entity\Playlist',
                'property' => 'name',
            ));
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title')
            ->add('playlist.name')
        ->add('video')->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array()
            )
        ))
        ;
    }
}

