<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Song;
use AppBundle\Entity\Playlist;
use JMS\Serializer\SerializationContext;
use AppBundle\Form\SongType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Route("/song")
 */
class SongController extends FOSRestController
{
    
    /**
     * @Rest\Get("/{playlistId}", requirements={"playlistId" = "\d+"})
     * @ApiDoc(
     *  resource=true,
     *  description="Returns a collection of Objects",
     *  section="Song",
     *  tags={
     *         "public" = "#89BF04"
     *     },
     *  output={
     *      "class"="AppBundle\Entity\Song"
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the songs are not found"
     *     },
     * requirements={
     *      {"name"="playlistId", "dataType"="integer", "requirement"="\d+","required"="true","description"="Unique Playlist Identifier"},
     *  }
     * )
     */
    public function getAction($playlistId)
    {    
        return $this->get('app.song.service')->getSongs($playlistId);
    }
    /**
     * @Rest\Get("/newest")
     * @ApiDoc(
     *  resource=true,
     *  description="Returns a newest collection of Objects",
     *  section="Song",
     *  tags={
     *         "public" = "#89BF04"
     *     },
     *  output={
     *      "class"="AppBundle\Entity\Song"
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the songs are not found"
     *     }
     * )
     * @Rest\QueryParam(
     *      name="limit",
     *      requirements="([0-1]?[0-9]|20)",
     *      default="20",
     *      description="Maximum number of results to return"
     *  )
     * @param integer $limit
     */
    public function getNewestAction($limit)
    {    
        return $this->get('app.song.service')->getNewestSongs($limit);
    }
    /**
     * @Rest\Post("/{playlistId}", requirements={"playlistId" = "\d+"})
     * @ApiDoc(
     *  resource=true,
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="JWT Token",
     *             "required"=true
     *         }
     *     },
     *  description="Create new Object.",
     *  section="Song",
     *  tags={},
     *  output={
     *      "class"="AppBundle\Entity\Song"
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the test is not found"
     *     },
     * requirements={
     *      {"name"="title", "dataType"="string", "required"="true", "description"="Song Title"},
     *      {"name"="video", "dataType"="string", "required"="true", "description"="Youtube Video Id"},
     *      {"name"="playlistId", "dataType"="integer", "requirement"="\d+", "required"="true", "description"="Unique Playlist Identifier"},
     *  }
     * )
     */
    public function addAction(Request $request, $playlistId)
    {
        $playlist = $this->get('app.access')->checkAccess($playlistId);
        $item = new Song();   
        $form = $this->createForm(new SongType(), $item);
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            return $this->get('app.song.service')->addSong($playlist,$item);
        }else {
            return View::create($form, 400);
        }
    }
    /**
     * @Rest\Delete("/{id}", requirements={"id" = "\d+"})
     * @ApiDoc(
     *  resource=true,
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="JWT Token",
     *             "required"=true
     *         }
     *     },
     *  description="Delete Object.",
     *  section="Song",
     *  tags={},
     *  output={
     *      "class"="AppBundle\Entity\Song"
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the song is not found"
     *     },
     * requirements={
     *      {"name"="id", "dataType"="integer", "required"="true", "requirement"="\d+" , "description"="Unique song identifier"},
     *      {"name"="playlistId", "dataType"="integer", "required"="true", "description"="Unique playlist identifier"},
     *  }
     * )
     */
    public function deleteAction(Request $request,$id)
    {
        $this->get('app.access')->checkAccess($request->get('playlistId'));
        $this->get('app.song.service')->deleteSong($id);
    }
}