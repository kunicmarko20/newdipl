<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Song;
use AppBundle\Entity\Playlist;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Form\PlaylistType;

/**
 * @Route("/playlist")
 */
class PlaylistController extends FOSRestController
{
    /**
     * @Rest\Post("/")
     * @ApiDoc(
     *  resource=true,
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="JWT Token",
     *             "required"=true
     *         }
     *     },
     *  description="Create new Object.",
     *  tags={},
     *  output={
     *      "class"="AppBundle\Entity\Playlist"
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the playlist is not found"
     *     },
     * requirements={
     *      {"name"="name", "dataType"="string", "required"="true", "description"="Playlist Title"},
     *  }
     * )
     */
    public function addAction(Request $request)
    {
        $item = new Playlist();
        $form = $this->createForm(new PlaylistType(), $item);
        $form->submit($request);

        if ($form->isValid()) {
            return $this->get('app.playlist.service')->managePlaylist($item);
        }else {
                return View::create($form, 400);
        }

    }
    /**
     * @Rest\Delete("/{id}", requirements={"id" = "\d+"})
     * @ApiDoc(
     *  resource=true,
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="JWT Token",
     *             "required"=true
     *         }
     *     },
     *  description="Delete Object.",
     *  tags={},
     *  output={
     *      "class"="AppBundle\Entity\Playlist"
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the playlist is not found"
     *     },
     * requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+","required"="true", "description"="Unique playlist identifier"}
     *  }
     * )
     */
    public function deleteAction(Request $request,$id)
    {
        $playlist = $this->get('app.access')->checkAccess($id);
        $this->get('app.playlist.service')->deletePlaylist($playlist);
    }
    /**
     * @Rest\Post("/reposition/{id}", requirements={"id" = "\d+"})
     * @ApiDoc(
     *  resource=true,
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="JWT Token",
     *             "required"=true
     *         }
     *     },
     *  description="Reposition songs in a playlist.",
     *  tags={},
     *  statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the playlist is not found"
     *     },
     * requirements={
     *      {"name"="id", "dataType"="integer", "required"="true", "requirement"="\d+", "description"="Unique playlist identifier"},
     *      {"name"="songs", "dataType"="array", "required"="true", "description"="Array of songs from the playlist."}
     *  }
     * )
     */
    public function repositionAction(Request $request,$id)
    {
        $this->get('app.access')->checkAccess($id);
        return  $this->get('app.playlist.service')->repositionPlaylist($request->get('songs'),$id);

    }

    /**
     * @Rest\Put("/{id}", requirements={"id" = "\d+"})
     * @ApiDoc(
     *  resource=true,
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="JWT Token",
     *             "required"=true
     *         }
     *     },
     *  description="Update Object.",
     *  tags={},
     *  output={
     *      "class"="AppBundle\Entity\Playlist"
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the playlist is not found"
     *     },
     * requirements={
     *      {"name"="name", "dataType"="string", "required"="true", "description"="Playlist Title"},
     *      {"name"="id", "dataType"="integer", "required"="true", "requirement"="\d+", "description"="Unique playlist identifier"}
     *  }
     * )
     */
    public function updateAction(Request $request,$id)
    {

        $item = $this->get('app.access')->checkAccess($id);

        $form = $this->createForm(new PlaylistType(), $item, array('method' => 'PUT'));
        $form->submit($request);

        if ($form->isValid()) {
            return $this->get('app.playlist.service')->managePlaylist($item,$request->get('hidden'));
        }else {
            return View::create($form, 400);
        }
    }
}
