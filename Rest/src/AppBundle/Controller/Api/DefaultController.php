<?php

namespace AppBundle\Controller\Api;
 
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DefaultController extends FOSRestController
{
    /**
     * @Rest\Get("/")
     */
    public function indexAction(Request $request)
    {
        $view = $this->view(['hello' => 'world'], Response::HTTP_OK);
        return $view;
    }
    /**
     * @Rest\Post("/register")
     */
    public function regAction(Request $request)
    {
    $userManager = $this->get('fos_user.user_manager');
    $email = $request->request->get('email');
    $username = $request->request->get('username');
    $password = $request->request->get('password');
    $email_exist = $userManager->findUserByEmail($email);
    $username_exist = $userManager->findUserByUsername($username);
    
    $response = new JsonResponse();
   
    if($email_exist || $username_exist){
        $response->setData("Username/Email ".$username."/".$email." exist");
        return $response;
    }

    $user = $userManager->createUser();
    $user->setUsername($username);
    $user->setEmail($email);
    $user->setLocked(false); 
    $user->setEnabled(true); 
    $user->setPlainPassword($password);
    $userManager->updateUser($user, true);

    $response->setData("User: ".$user->getUsername()." created");
    return $response;
    }
    /**
     * @Rest\Post("/rename")
     */
    public function renameAction(Request $request)
    {
     if($this->get('session')->get('rename') == $request->get('rename')){
         $userManager = $this->get('fos_user.user_manager');
         $user = $this->getUser();
         $username = $request->get('username');
         $username_exist = $userManager->findUserByUsername($username);
         if($username_exist){
                $response = new JsonResponse();
                $response->setData("Username ".$username." exist");
                $response->setStatusCode(409);
                return $response;
            }
    
            $user->setUsername($username);
            $userManager->updateUser($user);
            $this->get('session')->remove('rename');
            if ($user) {
                /** @var JWTManager $jwtManager */
                $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');
                /** @var EventDispatcher $dispatcher */
                $dispatcher = $this->get('event_dispatcher');

                $jwt = $jwtManager->create($user);
                $response = new JsonResponse();
                $event = new AuthenticationSuccessEvent(array('token' => $jwt), $user, $request,$response);
                $dispatcher->dispatch(Events::AUTHENTICATION_SUCCESS, $event);
                $response->setData($event->getData());

                return $response;
            }
      }else {
          return new Response('You can\'t do that',403);
      }
    }
}
