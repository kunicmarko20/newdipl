<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Entity\Playlist;
use JMS\Serializer\SerializationContext;
use AppBundle\Form\SongType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use AppBundle\Form\UserType;

/**
 * @Route("/user")
 */
class UserController extends FOSRestController
{ 
    /**
     * @Rest\Get("/{username}")
     * @ApiDoc(
     *  resource=true,
     *  description="Returns a collection of Objects",
     *  section="User",
     *  tags={
     *         "public" = "#89BF04"
     *     },
     *  output={
     *      "class"="AppBundle\Entity\User",
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the playlist is not found"
     *     },
     * requirements={
     *      {"name"="username", "dataType"="string","required"="true","description"="Unique username."},
     *  }
     * )
     */
    public function getAction($username)
    {
        return $this->get('app.user.service')->getUser($username);
    }
    
    /**
     * @Rest\Get("/search/{string}")
     * @ApiDoc(
     *  resource=true,
     *  description="Returns User Info and playlists",
     *  section="User",
     *  tags={
     *         "public" = "#89BF04"
     *     },
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the playlist is not found"
     *     },
     * requirements={
     *      {"name"="string", "dataType"="string","required"="true","description"="Search playlists or usernames."},
     *  }
     * )
     * @Rest\QueryParam(
     *      name="page",
     *      requirements="(\d+)",
     *      default="1",
     *      description="Page number"
     *  )
     * @param integer $limit
     */
    public function searchAction($string,$page)
    {
        return $this->get('app.user.service')->search($string,$page);
    }
    
        
    /**
     * @Rest\Put("/{username}")
     * @ApiDoc(
     *  resource=true,
     *  headers={
     *         {
     *             "name"="Authorization",
     *             "description"="JWT Token",
     *             "required"=true
     *         }
     *     },
     *  description="Update User.",
     *  section="User",
     *  tags={},
     *  output={
     *      "class"="AppBundle\Entity\User",
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized",
     *         404="Returned when the playlist is not found"
     *     },
     * requirements={
     *      {"name"="username", "dataType"="string", "required"="true", "description"="Users username"}
     *  }
     * )
     */
    public function updateAction(Request $request,$username)
    {
        
        $item = $this->get('app.user.service')->checkUser($username);
        if($item[0]){
            $form = $this->createForm(new UserType(), $item[1], array('method' => 'PUT'));
        }else {
            throw new AccessDeniedHttpException('You don\'t have access to do that');
        }
        $form->submit($request);
        
        if ($form->isValid()) {
            return $this->get('app.user.service')->editUser($item[1],$request->get('hidden'));
        }else {
            return View::create($form, 400);
        }
    }
}