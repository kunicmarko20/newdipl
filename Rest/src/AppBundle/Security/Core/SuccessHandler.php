<?php

namespace AppBundle\Security\Core;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

class SuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var JWTManager
     */
    protected $jwtManager;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param JWTManager               $jwtManager
     * @param EventDispatcherInterface $dispatcher
     */
    protected $session;


    public function __construct(JWTManager $jwtManager, EventDispatcherInterface $dispatcher,Session $session)
    {
        $this->jwtManager = $jwtManager;
        $this->dispatcher = $dispatcher;
        $this->session = $session;
    }

    /**
     * {@inheritDoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();
        $jwt  = $this->jwtManager->create($user);

        $response = new JsonResponse();
        $event    = new AuthenticationSuccessEvent(['token' => $jwt], $user, $request, $response);

        $this->dispatcher->dispatch(Events::AUTHENTICATION_SUCCESS, $event);
        if(!$this->session->get('rename')){
            return new RedirectResponse('http://new.dipl.dev/login?token='.$event->getData()['token']."&user=".$user->getUsername());
        }
        return new RedirectResponse('http://new.dipl.dev/login?token='.$event->getData()['token']."&user=".$user->getUsername()."&rename=".$this->session->get('rename'));
    }
}
