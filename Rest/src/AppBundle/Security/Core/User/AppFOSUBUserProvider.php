<?php

namespace AppBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Doctrine\UserManager;

class AppFOSUBUserProvider extends BaseFOSUBProvider
{
    private $session;
    public function __construct(UserManager $userManager, Array $properties, Session $session)
    {
        $this->session=$session;
        parent::__construct($userManager, $properties);
    }
    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $email = $response->getEmail();
        $username = explode("@",$email)[0];
        $user = $this->userManager->findUserBy(array('email' => $email));
        $this->session->remove('rename');
        if (null === $user) {
            $service = $response->getResourceOwner()->getName();
            $setter = 'set'.ucfirst($service);
            $setter_id = $setter.'Id';
            $setter_token = $setter.'AccessToken';
            // create new user here
            $user = $this->userManager->createUser();
            $user->$setter_id($response->getUsername());
            $user->$setter_token($response->getAccessToken());
            $user->setUsername($username);
            $user->setEmail($email);
            $pw = md5($username.time());
            $user->setPassword($pw);
            $user->setEnabled(true);
            $this->userManager->updateUser($user);
            $rand = md5(mt_rand(10,100));
            $this->session->set('rename', $rand);
            return $user;
        }
        
        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';
        //update access token
        $user->$setter($response->getAccessToken());
        return $user;
    }
    
    
}

