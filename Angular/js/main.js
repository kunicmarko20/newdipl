var vid ;
var func = true;
$(document).ready(function(){
    jwplayer("myElement").setup({
        file:'http://www.youtube.com/watch?v=KMU0tzLwhbE',
        mediaid:'1',
        width: 900,
        height: 30,
        events:{
            onComplete: function() {
                var bla = $("#now-playing-songs a[data-href='" + vid +"']").parent().next();
                if(bla.find("a").length > 0){
                    $(bla).find("a")[0].click();
                    $('.my-custom-player-button').show();
                }
            }
        }
    });

    jwplayer('myElement').onPlay(function(){ jwplayer("myElement").setVolume(jwplayer("myElement").getVolume()); $('#now-playing-songs .col-md-4').find('span').remove();$("#now-playing-songs a[data-href='" + vid +"']").parent().find("img").after('<span class="button-span-def glyphicon glyphicon-pause"></span>'); });

    jwplayer('myElement').onPause(function(){$('#now-playing-songs .col-md-4').find('span').remove();$("#now-playing-songs a[data-href='" + vid +"']").parent().find("img").after('<span class="button-span-def glyphicon glyphicon-play"></span>'); });

    jwplayer("myElement").on('ready',function(){
        $('.jw-icon-playback').after('<span style="margin-right:10px;" onclick="prev()" class="control-bar my-custom-player-button jw-button-color glyphicon glyphicon-backward"></span><span style="margin-right:10px;" title="Now Playing" data-toggle="modal" data-target="#nowPlayingModal" class="control-bar my-custom-player-button jw-button-color glyphicon glyphicon-menu-hamburger"></span><span onclick="next()" class="control-bar my-custom-player-button jw-button-color glyphicon glyphicon-forward"></span>');
        $('.jw-slider-time').css({'padding-top':'7px','width':'85%','float':'left'});
        $('.jw-slider-time').after("<div class='now-playing'><div class='now-playing-text'></div></div>");
        $('.jw-slider-volume').css('padding-top','1px');
    });

})

function playTrailer(video,title,ovo){
    $("#player").fadeIn();
    if(!$(ovo).data('in-playlist')){
        $('.my-custom-player-button').hide();
    }
    if(video != vid){
        $(".now-playing-text").fadeOut(function() {
          $(".now-playing-text").text(title)
        }).fadeIn();
        func && nowPlaying();

        jwplayer("myElement").load([{
            file: video,
            mediaid: '1',
            width: 900,
            height: 30,
        }]);
        vid = video;
        jwplayer("myElement").play();}
    else {
        jwplayer("myElement").pause();
  }
}
function nowPlaying(){
    var marquee = $('.now-playing');
    marquee.each(function() {
        var mar = $(this),indent = mar.width();
        mar.marquee = function() {
            indent--;
            mar.css('text-indent',indent);
            if (indent < -1 * mar.children('.now-playing-text').width()) {
                indent = mar.width();
            }
        };
        mar.data('interval',setInterval(mar.marquee,35));
    });
    func = false;
}

function next(){
    var next = $("#now-playing-songs a[data-href='" + vid +"']").parent().next();

    if(next.find("a").length > 0){
        $(next).find("a")[0].click();
        $('.my-custom-player-button').show();
    }
}

function prev(){
    var prev = $("#now-playing-songs a[data-href='" + vid +"']").parent().prev();

    if(prev.find("a").length > 0){
        $(prev).find("a")[0].click();
        $('.my-custom-player-button').show();
    }
}