

app.controller('PlaylistController', function ($scope, globalService, $http, $location, $compile, $templateCache, $route, $routeParams) {


    var currentPageTemplate;
     init();
    function init() {
        $('.modal-sm .modal-footer button:first').attr("id","btn-submit-playlist");
        $compile($('#btn-submit-playlist'))($scope);
        getPlaylists();
        currentPageTemplate = $route.current.templateUrl;

    }

    function getPlaylists(){
        $http({url: $scope.apiurl+'api/user/'+$routeParams.name,method: "GET"}).then(function successCallback(response) {
            $scope.userPlaylist = response.data.playlists;
            $scope.userInfo = response.data.user;
            $scope.showdata = true;
  }, function errorCallback() {
    console.clear();
    $location.path("/");
  });
  }
    $scope.newPlaylist = function(){
        $('.modal-sm .modal-title').text('Add New Playlist');
        $('.modal-sm .modal-body').html('<div class="form-group text-center"></div>');
        $('.modal-sm .modal-body .form-group').append('<input type="text" class="form-control" placeholder="Playlist Name" id="playlist-name"/><br/>Private Playlist : <input type="checkbox" id="private-playlist"/>');
        $('#playlist-name').after('<input type="file" id="playlist-picture" class="form-control">');
        $('#btn-submit-playlist').show();
        $scope.playlistoption = 'new';
    };
    $scope.delSong = function(event,id){
        $(event.target).parent().toggle( "clip", function() { $(event.target).remove(); });
        $http({
                  method: "DELETE",
                  url: $scope.apiurl+"api/song/"+id,
                  data: $.param({'playlistId':pl}),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
    };
    $scope.userEdit = function(){
            var bio = $('#user-bio').val();
            var file = $('#user-picture')[0].files[0];
            var old = $('#user-old-pic').val();
            var formData = new FormData();

            if( file == undefined ) {
                    formData.append('user[picture]', old);
                }else{
                    formData.append('user[picture]', file);
                    formData.append('hidden', old);
                }
            formData.append('user[bio]', bio);
         $http({
                        method: "POST",
                        url: $scope.apiurl+"api/user/"+$routeParams.name,
                        data: formData,
                        enctype: 'multipart/form-data',
                        processData: false,
                        cache: false,
                        processData: false,
                        headers: {'Content-Type': undefined,"X-HTTP-Method-Override": "PUT"}
                     }).
                      success(function(response) {
                         $('#userModal').modal('hide');
                         $('#userModal').on('hidden.bs.modal', function () {
                                $templateCache.remove(currentPageTemplate);
                                $route.reload();
                          });

                     });
    }
    $scope.smModal = function(){
            var playlistn = $('#playlist-name').val();
            var privateplaylist = $('#private-playlist').is(":checked");
            var file = $('#playlist-picture')[0].files[0];
            var formData = new FormData();

            if(playlistn != "" && playlistn != null){
                if( privateplaylist == true ) formData.append('playlist[private]', true);
                if( file !== undefined ) formData.append('playlist[picture]', file);
                formData.append('playlist[name]', playlistn);
                if( $scope.playlistoption == 'new'){
                    $http({
                           method: "POST",
                            url: $scope.apiurl+"api/playlist/",
                            data: formData,
                            enctype: 'multipart/form-data',
                            processData: false,
                            cache: false,
                            processData: false,
                            headers: {'Content-Type': undefined}
                         }).
                          success(function(response) {
                             $('#smModal').modal('hide');
                             $('#smModal').on('hidden.bs.modal', function () {
                                $templateCache.remove(currentPageTemplate);
                                $route.reload();
                            });
                         });
                }else {
                    var pic = jQuery("a[data-pid='" + pl +"'] img").attr('src').split("/");
                    pic = pic.pop();
                    if( file == undefined ) {
                         formData.append('playlist[picture]', pic);
                    }else{
                         formData.append('hidden', pic);
                    }
                     $http({
                        method: "POST",
                        url: $scope.apiurl+"api/playlist/"+pl,
                        data: formData,
                        enctype: 'multipart/form-data',
                        processData: false,
                        cache: false,
                        processData: false,
                        headers: {'Content-Type': undefined,"X-HTTP-Method-Override": "PUT"}
                     }).
                      success(function(response) {
                         $('#smModal').modal('hide');
                         $('#lgModal').modal('hide');
                         $('#lgModal').on('hidden.bs.modal', function () {
                                $templateCache.remove(currentPageTemplate);
                                $route.reload();
                          });

                     });
                }

            }else{
              $('#playlist-name').css('border','1px solid red');
            }
    };
    $scope.openPlayist = function(event){
            globalService.open(event);
            $compile($('#edit-playlist-title'))($scope);
            $scope.loadSongs(true);
       };
    $scope.loadSongs = function( open = false ){
            $http({
                  method: "GET",
                  url: $scope.apiurl+"api/song/"+pl,
                  headers: {'Content-Type': 'application/x-www-form-urlencoded',headers: {'Authorization': undefined}}
                }).
                 success(function(response) {
                    var style = 'max-height: 500px;';
                    if(response){
                        if(response.length > 6){
                            style = 'overflow-y:scroll;max-height: 500px;';
                        }
                        $('#lgModal .modal-body').html('<div class="container" style="'+style+'"><div id="origin" class="col-md-12"></div></div>');

                        $.each( response, function( key, s ) {
                            var title = s.title;
                            if(title.length > 45) {
                                title = title.substring(0,44)+"...";
                            }
                          $('#origin').append('<div class="col-md-4 text-center"><button type="button" ng-if="checkAccess()" ng-click="delSong($event,'+s.id+');" class="close btn-del-song">&times;</button><a href="javascript:void(0)" onclick="playTrailer(\'http://www.youtube.com/watch?v='+s.video+'\',\''+s.title+'\',this)" title="'+s.title+'" data-href="http://www.youtube.com/watch?v='+s.video +'"><img src="https://i.ytimg.com/vi/'+s.video +'/mqdefault.jpg"><br><h4>'+ title +'</h4></a></div>');
                        });
                        $compile($('.btn-del-song'))($scope);
                        $.getScript('/js/sort.js');
                    }
                    open && $('#lgModal').modal('toggle');
                });

    };
    $scope.playPlaylist = function(){
        globalService.play();
    };
    $scope.lgModal = function(event){
        var pls = $('#btn-del-playlist').data('playlist');
        var uppos = [];
        $("#origin .col-md-4 a").each(function( ) {
            uppos.push($(this).data('href').split('=')[1]);
        });
        if (uppos.length > 0) {
            $http({
                  method: "POST",
                  url: $scope.apiurl+"api/playlist/reposition/"+pls,
                  data: $.param({'songs': uppos}),
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).
                 success(function(response) {
                });
        }

    }


    $scope.editPlaylist = function(event = false){
        $scope.newPlaylist();
        $('.modal-sm .modal-title').text('Edit Playlist');
        $scope.playlistoption = 'edit';
        $('#playlist-name').val($('#lgModal h4.pull-left-title').text());
    }
    $scope.addSong = function(event){
        $('.modal-sm .modal-title').text('Add New Song');
        $('.modal-sm .modal-body').html('<div class="form-group"></div>');
        $('.modal-sm .modal-body .form-group').append('<label for="Search">Search:</label><input type="text" class="form-control" id="search"/>');
        $('.modal-sm .modal-body').append('<div id="search-res" class="list-group" style="overflow-y:scroll;max-height: 345px;display:none;"></div>');
        $('#btn-submit-playlist').hide();
        $.getScript('/js/search.js');
        $('#smModal').on('hidden.bs.modal', function () {
            $('.modal-sm .modal-body').html('');
            $scope.loadSongs();
        });
    }
    $scope.delPlaylist = function(event){
                    var id = $(event.target).data('playlist');
                    $http({
                      method: "DELETE",
                      url: $scope.apiurl+"api/playlist/"+id,
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(response) {
                        $('#lgModal').modal('hide');
                        $('#lgModal').on('hidden.bs.modal', function () {
                            $templateCache.remove(currentPageTemplate);
                            $route.reload();
                        });
                    });

    }
    $('#lgModal').on('hidden.bs.modal', function () {
          $('#lgModal .modal-body').html('');
    });
    $('#smModal').on('hidden.bs.modal', function () {
          $('.modal-sm .modal-body').html('');
          if($('#smModal .modal-title:contains("Song")').length){
            $scope.loadSongs();
          }

    });

});

app.controller('LoginController', function ($scope, $http, $location,$routeParams) {
    localStorage.removeItem("token");

    var param1 = $routeParams.token;
    var param2 = $routeParams.user;
    var param3 = $routeParams.rename;
    if(typeof param1 !== "undefined" && typeof param2 !== "undefined"){
       localStorage.setItem("token", "Bearer "+param1);
       localStorage.setItem("username",param2);
       console.clear();
       $http.defaults.headers.common.Authorization=localStorage.getItem("token");
       $location.url($location.path());
       if(typeof param3 !== "undefined"){
            $location.path("/rename/"+param3);
       }else{
            $location.path("/profile/"+param2);
       }
    }


    $scope.Login = function(){
        var username = $("#username").val();
        var password = $("#password").val();
        if(username != '' && password != ''){
            $http({
                              method: "POST",
                              url: $scope.apiurl+"api/login_check",
                              data: $.param({'_username':username,'_password':password}),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function(response) {
                                localStorage.setItem("token", "Bearer "+response.token);
                                localStorage.setItem("username",username);
                                console.clear();
                                $http.defaults.headers.common.Authorization=localStorage.getItem("token");
                                $location.path("/profile/"+username);
                            }).error(function () {
                            localStorage.clear();
                            $scope.flash = {danger:"Bad credentials"};
                            $("#password").val('');
                        });
            }
        }


});
app.controller('RegisterController', function ($scope, $http, $location,$routeParams) {
    $scope.Register = function(){
        var username = $("#username").val();
        var password = $("#password").val();
        var email = $("#email").val();
        if(username != '' && password != '' && email != ''){
            $http({
                              method: "POST",
                              url: $scope.apiurl+"api/register",
                              data: $.param({'username':username,'password':password,'email':email}),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function(response) {
                                $location.path("/login");
                            });
            }
        }


});
app.controller('RenameController', function ($scope, $http, $location,$routeParams, $rootScope) {
    $("#username").val(localStorage.getItem("username"));
    $scope.Rename = function(){
        var username = $("#username").val();
        if(username != ''){
            $http({
                              method: "POST",
                              url: $scope.apiurl+"api/rename",
                              data: $.param({'username':username,'token':$routeParams.token}),
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function(response) {
                                localStorage.setItem("token", "Bearer "+response.token);
                                localStorage.setItem("username",username);
                                console.clear();
                                $http.defaults.headers.common.Authorization=localStorage.getItem("token");
                                $location.path("/profile/"+username);
                            }).error(function(data, status, headers, config){
                                if(status == 409){
                                   $rootScope.flash = {danger:data};
                                }
                            });
            }
        }
});
app.controller('IndexController', function ($scope, $http, $location,globalService) {

    getNewest();
    function getNewest(){
        $http({url: $scope.apiurl+'api/song/newest?limit=6',method: "GET",headers: {'Authorization': undefined}}).then(function(response) {
            $.each( response.data, function( key, s ) {
                var title = s.title;
                if(title.length > 45) {
                    title = title.substring(0,44)+"...";
                }
                $('#origin').append('<div class="col-md-4 text-center"><a href="javascript:void(0)" onclick="playTrailer(\'http://www.youtube.com/watch?v='+s.video+'\',\''+s.title+'\',this)" title="'+s.title+'" data-href="http://www.youtube.com/watch?v='+s.video +'"><img src="https://i.ytimg.com/vi/'+s.video +'/mqdefault.jpg"><br><h4>'+ title +'</h4></a></div>');
                $scope.index = true;
            });
        });
    }
    $scope.playPlaylist = function(){
        globalService.play();
    }
});
app.controller('SearchController', function ($scope, $http, $location,$routeParams, $rootScope,$compile,globalService) {
            if(jQuery('#search-term').val() == ''){
                jQuery('#search-term').val($routeParams.string);
            }
            $http({
                              method: "GET",
                              url: $scope.apiurl+"api/user/search/"+$routeParams.string,
                              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }).success(function(response) {
                                $scope.searchResults = response.result;
                                $scope.searchPages = response.pages;
                                $scope.search = true;
                            });
    $scope.searchRender = function(z){
        return Object.keys(z).length;
    }
    $scope.searchPagination = function(event,n){
        jQuery('.pagination li').removeClass('active');
        jQuery(event.target).parent().addClass('active');
       $http({
        method: "GET",
        url: $scope.apiurl+"api/user/search/"+$routeParams.string+"?page="+n,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function(response) {
          $scope.searchResults = response.result;
          $scope.searchPages = response.pages;
          $scope.search = true;
      });  
    }
    $scope.openPlayist = function(event){
            globalService.open(event);
            $compile($('#edit-playlist-title'))($scope);
            $scope.loadSongs(true);
       };
    $scope.loadSongs = function( open = false ){
            $http({
                  method: "GET",
                  url: $scope.apiurl+"api/song/"+pl,
                  headers: {'Content-Type': 'application/x-www-form-urlencoded',headers: {'Authorization': undefined}}
                }).
                 success(function(response) {
                    var style = 'max-height: 500px;';
                    if(response){
                        if(response.length > 6){
                            style = 'overflow-y:scroll;max-height: 500px;';
                        }
                        $('#lgModal .modal-body').html('<div class="container" style="'+style+'"><div id="origin" class="col-md-12"></div></div>');

                        $.each( response, function( key, s ) {
                            var title = s.title;
                            if(title.length > 45) {
                                title = title.substring(0,44)+"...";
                            }
                          $('#origin').append('<div class="col-md-4 text-center"><button type="button" ng-if="checkAccess()" ng-click="delSong($event,'+s.id+');" class="close btn-del-song">&times;</button><a href="javascript:void(0)" onclick="playTrailer(\'http://www.youtube.com/watch?v='+s.video+'\',\''+s.title+'\',this)" title="'+s.title+'" data-href="http://www.youtube.com/watch?v='+s.video +'"><img src="https://i.ytimg.com/vi/'+s.video +'/mqdefault.jpg"><br><h4>'+ title +'</h4></a></div>');
                        });
                        $compile($('.btn-del-song'))($scope);
                        $.getScript('/js/sort.js');
                    }
                    open && $('#lgModal').modal('toggle');
                });

    };
    $scope.playPlaylist = function(){
        globalService.play();
    };
});
