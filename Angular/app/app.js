var pl;
var app = angular.module('ytApp', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/profile/:name',
            {
                controller: 'PlaylistController',
                templateUrl: '/app/partials/profile.html'
            })
        .when('/login/:token?/:user?/:rename?',
            {
                controller: 'LoginController',
                templateUrl: '/app/partials/login.html',
                resolve: {
                    factory: checkLogin
                }
            })
        .when('/register',
            {
                controller: 'RegisterController',
                templateUrl: '/app/partials/register.html',
                resolve: {
                    factory: checkLogin
                }
            })
        .when('/rename/:token',
            {
                controller: 'RenameController',
                templateUrl: '/app/partials/rename.html'
            })
        .when('/search/:string',
            {
                controller: 'SearchController',
                templateUrl: '/app/partials/search.html'
            })
        .when('/',
            {
                controller: 'IndexController',
                templateUrl: '/app/partials/index.html'
            })
        .otherwise({ redirectTo: '/' });

        $locationProvider.html5Mode(true);
});
app.config(['$httpProvider', function ($httpProvider ) {
    $httpProvider.defaults.headers.common.Authorization=localStorage.getItem("token");
    $httpProvider.interceptors.push(function($q, $rootScope, $location) {
        return {
          'responseError': function(response) {
              if(response.status == 401){
                  $httpProvider.defaults.headers.common.Authorization = undefined;
                  localStorage.removeItem("token");
                  $rootScope.logshow = true;
                  $rootScope.flash = {danger:"Please Login"};
                  jQuery('.modal-backdrop').remove();
                  $location.path("/login");
              }
             return $q.reject(response);
          }
        };
      });
}]);

app.filter('unsafe', function($sce) { return $sce.trustAsHtml; });

var checkLogin= function ($rootScope, $q, $location, $http) {
    var deferred = $q.defer();
        if($rootScope.logshow || !localStorage.getItem("token")){
          deferred.resolve(true);
          return deferred.promise;
        }
        deferred.reject();
        $location.path("/");
        return deferred.promise;
};
app.filter('makeRange', function() {
        return function(input) {
            var result = [];
            for (var i = 1; i <= input; i++)
                result.push(i);
            return result;
        };
});
app.factory('globalService', function($http,$compile) {
        return {
            play: function() {
              $('#now-playing-songs').html($('#origin').children().clone());
                $('#now-playing-songs .col-md-4 a').data('in-playlist',true);
                $('#lgModal').modal('hide');
                if($('#now-playing-songs').children().length > 6){
                    $("#now-playing-songs").css({"max-height":"500px", "overflow-y":"scroll"});
                }else {
                    $("#now-playing-songs").removeAttr("style");
                    $("#now-playing-songs").css({"max-height":"500px"});
                }
                $.getScript('/js/sort.js');
                $('#now-playing-songs .col-md-4:first a')[0].click();
                setTimeout(function(){ jwplayer("myElement").play(); }, 100);
                $('.my-custom-player-button').show();

                $(".btn-del-song").click(function(){
                    $(this).parent().toggle( "clip", function() { $(this).remove(); });
                });  
            },
            open: function(event){
               $('#lgModal .modal-header span').remove();
                    $('#lgModal .modal-title').addClass('pull-left-title').text($(event.target).parent().attr('title'));
                    $('#lgModal .modal-title').after('<span role="button" ng-if="checkAccess()" id="edit-playlist-title" ng-click="editPlaylist($event)" data-toggle="modal" data-target="#smModal" class="position-pencil glyphicon glyphicon-pencil"></span>');
                    pl = $(event.target).parent().data('pid');
                    $('#btn-del-playlist').attr('data-playlist',pl); 
            }
        };
    });

app.controller('AppController', function ($scope,$location,$routeParams,$compile,$http,$rootScope) {
    $scope.apiurl = "http://api-kunicmarko20.rhcloud.com/";
    $scope.isAuth = function(){
        return localStorage.getItem("token") ? localStorage.getItem("username"): '';
    };
    $scope.showhide = function(event){
        $('.'+event).removeClass(event);
    };
    $scope.Logout = function(){
        $("#loader").parent().show();
        setTimeout(function(){
            localStorage.removeItem("token");
            localStorage.removeItem("username");
            $("#loader").parent().hide();
            window.location = "/";
        }, 500);
    };
    $scope.$watch('search',function(oldVal){
        var path = '/search/'+oldVal;
        if(oldVal !== undefined){
            if(oldVal.length > 2){
                $location.path(path);
            }else{
                $location.path("/");
            }
        }
    });
    $scope.checkAccess = function(){
        return $routeParams.name === localStorage.getItem("username") ? true : false;
    };
     $rootScope.$on("$locationChangeStart", function(event, next, current) { 
         var route;
         if(next.match(/(profile)/g)){
             route = 'profile';
         }else if(next.match(/(search)/g)){
             route = 'search';
         }else{
            route = next.split('/');
            route = route.pop();
        }
        $scope.contactShow = false;
        switch(route){
            case 'login':
                $scope.rTitle = 'Login';
                $scope.rDescription = 'Your playlists are waiting';
            break;
            case 'register':
                $scope.rTitle = 'Register';
                $scope.rDescription = 'Create your personal and private playlists';
            break;
            case 'profile':
               $scope.rTitle = 'Profile';
               var us = next.split('/').pop();
               $scope.rDescription = 'Welcome to '+us+'\'s profile';
            break;
            case 'search':
                $scope.rTitle = 'Search';
                var us = next.split('/').pop();
                $scope.rDescription = 'Term: '+us;
            break;
            case 'rename':
                $scope.rTitle = 'Rename';
                $scope.rDescription = 'Choose your username';
            break;
            default:
                $scope.rTitle = 'Music Fly';
                $scope.rDescription = 'All your music at one place';
                $scope.contactShow = true;
            break;
        }
      });
});